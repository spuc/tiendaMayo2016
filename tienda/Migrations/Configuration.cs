namespace tienda.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using tienda.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<tienda.Models.PeliculaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(tienda.Models.PeliculaDBContext context)
        {
            Random rnd = new Random(); 
            for (int i = 0; i < 100; i++)
            {
                context.peliculas.AddOrUpdate(
                  p => p.Titulo,
                  new Pelicula { 
                      Titulo = "Jurassik Park"+i, 
                      Director = "Steven Spielberg",
                      Fecha = DateTime.Parse("1993-1-11") ,
                      ruta = "/Content/imagenes/HP" +
                            rnd.Next(1, 5).ToString() + ".jpg",
                      Rate = rnd.Next(1, 5)
                  }
                );
            }
            
        }
    }
}
