﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tienda.Models;
using PagedList;
using System.IO;

namespace tienda.Controllers
{
    public class PeliculaController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        //
        // GET: /Pelicula/
        private int paginacion = 10;

        public ActionResult Index(int pagina = 1)
        {
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult Lista(int pagina = 1)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Resultados", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult ListaImagenes(int pagina = 1)
        {
            var paginacion = 18;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ResultadosImagenes", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult ListaPopOverImagenes(int pagina = 1)
        {
            var paginacion = 18;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ResultadosPopOverImagenes", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }


        //
        // GET: /Pelicula/Details/5

        public ActionResult Details(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // GET: /Pelicula/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pelicula/Create

        [HttpPost]
        public ActionResult Create(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);
                    
                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }

                db.peliculas.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pelicula);
        }

        //
        // GET: /Pelicula/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Edit/5

        [HttpPost]
        public ActionResult Edit(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);

                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;

                }

                db.Entry(pelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }

        //
        // GET: /Pelicula/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            db.peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RateProduct(int id, int rate)
        {
            int userId = 142; 
            bool success = false;
            string error = "";
            try
            {
                success = CalificarPelicula(userId, id, rate);
            }
            catch (System.Exception ex)
            {
                error = ex.Message;
            }
            return Json(new { error = error, success = success, pid = id }, 
                JsonRequestBehavior.AllowGet);
        }

        private bool CalificarPelicula(int userId, int id, int rate)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return false;
            }
            pelicula.Rate = rate;
            db.Entry(pelicula).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public ContentResult AddFavorite(int id)
        {
            List<int> favorites = Session["Favorites"] as List<int>;
            if (favorites == null)
            {
                favorites = new List<int>();
            }
            favorites.Add(id);
            Session["Favorites"] = favorites;
            return Content("La pelicula fue agregada", "text/plain", System.Text.Encoding.Default);
        }

        public ActionResult FavoritesSlideshow()
        {
            List<Pelicula> favPhotos = new List<Pelicula>();
            List<int> favoriteIds = Session["Favorites"] as List<int>;
            if (favoriteIds == null)
            {
                favoriteIds = new List<int>();
            }
            Pelicula currentMovie;

            foreach (int favID in favoriteIds)
            {
                currentMovie = db.peliculas.Find(favID);
                if (currentMovie != null)
                {
                    favPhotos.Add(currentMovie);
                }
            }

            return View("SlideShow", favPhotos);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}