﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tienda.Models;

namespace peliculas.Controllers
{

    public class CommentController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();
               
        
        [ChildActionOnly] //This attribute means the action cannot be accessed from the browser's address bar
        public PartialViewResult _CommentsForMovie(int id = 0)
        {
            
            var comments = from c in db.Comments
                           where c.MovieId == id
                           select c;
            
            ViewBag.MovieId = id;
            return PartialView(comments.ToList());
        }

        //
        //POST: This action creates the comment when the AJAX comment create tool is used
        [HttpPost]
        public PartialViewResult _CommentsForMovie(Comment comment, int id = 0)
        {

            //Save the new comment
            comment.MovieId = id;
            db.Comments.Add(comment);
            db.SaveChanges();

            //Get the updated list of comments
            var comments = from c in db.Comments
                           where c.MovieId == id
                           select c;
            //Save the MovieId in the ViewBag because we'll need it in the view
            ViewBag.MovieId = id;
            //Return the view with the new list of comments
            return PartialView("_CommentsForMovie", comments.ToList());
        }

        //
        // GET: /Comment/_Create. A Partial View for displaying the create comment tool as a AJAX partial page update
        [Authorize]
        public PartialViewResult _Create(int id = 0)
        {
            //Create the new comment
            Comment newComment = new Comment();
            newComment.MovieId = id;
            newComment.UserName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(User.Identity.Name);

            ViewBag.MovieId = id;
            return PartialView("_CreateAComment");
        }



        //
        // GET: /Comment/Delete/5
        [Authorize]
        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            Pelicula pelicula = db.peliculas.Find(comment.MovieId);
            comment.pelicula = pelicula;


            ViewBag.MovieId = comment.MovieId;
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
           
                     
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("details", "pelicula", new { id = comment.MovieId });
           
        }

    }
}
