﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tienda.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int MovieId { get; set; }
        public string UserName { get; set; }
        
        [Required]
        [StringLength(250)]
        public string Subject { get; set; }

        [DataType(DataType.MultilineText)]
        public string Body { get; set; }

        public virtual Pelicula pelicula { get; set; }

    }
}